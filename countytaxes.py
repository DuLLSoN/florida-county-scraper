# -*- coding: utf-8 -*-
import requests
from lxml import html
from time import sleep
import logging
logging.basicConfig(level=logging.INFO, format='%(asctime)s %(name)s [%(levelname)s] - %(message)s')
logging.getLogger("requests").setLevel(logging.WARNING)

class CountyTaxes(object):
	"""docstring for CountyTaxes"""
	def __init__(self, section, loghandler=None, proxyhandler=None):
		super(CountyTaxes, self).__init__()
		self.log = logging.getLogger("CountyTaxes")
		if loghandler:
			self.log.addHandler(loghandler)
		if proxyhandler:
			self.setProxyHandler(proxyhandler)
		self.setSection(section)

	def setProxyHandler(self, ph):
		self.proxyhandler = ph.copy()
		self.requestGet = self.proxyhandler.request_get
		self.proxyhandler.request_filter = self.requestFilter

	def setSection(self, section):
		self.section = section

	def getPropertyTax(self, folio):
		if self.section is None:
			raise Exception("SECTION_NONE")
		self.log.info("Folio No: %s - Getting tax info...", folio)
		page = self.getPage(self.section, folio)
		data = self.parsePropertyTax(page)
		data["Folio"] = folio
		return data

	def parsePropertyTax(self, content):
		tree = html.fromstring(content)
		totaltax = tree.xpath('//span[@class="mix_of_payable_and_unpayable" and contains(text(),"Account")]/text()')
		deeddate = tree.xpath('//td[following-sibling::td[position()=1 and text()="Deed sale"]]/text()')
		data = {
			"Total Tax":"${}".format(totaltax[0].split('$')[1]) if totaltax else "",
			"Deed Date":deeddate[0] if deeddate else "",
			}
		return data

	def getPage(self, section, folio):
		r = self.requestGet("https://{}.county-taxes.com/public/real_estate/parcels/{}/bills".format(section, folio))
		if r == None:
			self.log.error("Problem while getting {} - {}".format(section, folio))
			raise Exception("REQUEST_NONE")
		filter = self.requestFilter(r)
		if filter is not "OK":
			raise Exception(filter)
		return r.text

	def requestGet(self, *args, **kwargs):
		h = kwargs.get("headers",{})
		h.update({'User-Agent': (
					"Mozilla/5.0 (Windows NT 10.0; Win64; x64) "
					"AppleWebKit/537.36 (KHTML, like Gecko) "
					"Chrome/56.0.2924.87 Safari/537.36")})
		kwargs["headers"] = h
		page = ''
		while page == '':
			try:
				page = requests.get(*args, **kwargs)
			except requests.exceptions.ConnectionError:
				self.log.error("Server refused connection, retrying...")
				sleep(1)
		return page

	def requestFilter(self, req):
		if "Access denied" in req.text:
			return "LIMIT_EXCEEDED"
		if "TaxSys" not in req.text:
			return "BAD_RESPONSE"
		return "OK"
