# -*- coding: utf-8 -*-
from gevent import monkey, sleep

monkey.patch_all()

import requests
import re
from lxml import html

from basescraper import BaseScraper


class LeecountyScraper(BaseScraper):
    def __init__(self, loghandler=None, proxyhandler=None):
        super(LeecountyScraper, self).__init__(loghandler=loghandler, proxyhandler=proxyhandler)
        self.viewstate = ""
        self.headers = ["Folio", "Owner Name", "Owner Address", "Site Address", "Classification", "Sale Date",
                        "Sale Price"]
        self.data = []
        self.updateViewState()

    def updateViewState(self):
        page = self.requestGet("http://www.leepa.org/Search/PropertySearch.aspx")
        match = re.search('__VIEWSTATE" value="(.*?)"', page.text)
        if match:
            self.viewstate = match.group(1)
        else:
            self.log.error("Could not update the viewstate")

    def getPropertyInfoAndTax(self, folio):
        folio_id = self.getFolioId(folio)
        page = self.getPropertyInfo(folio_id)
        item = self.parsePropertyInfo(page.text)
        item["Folio"] = folio;
        return item

    def getPropertyInfo(self, folio_id):
        self.log.info("Parsing the property info of %s", folio_id)
        page = self.requestGet(
            "http://www.leepa.org/Display/DisplayParcel.aspx",
            params={
                "FolioID": folio_id,
                "SalesDetails": "True",
            }
        )
        return page

    def parsePropertyInfo(self, text):
        tree = html.fromstring(text)
        owner_text = tree.xpath('//div[@id="ownersDiv"]/following-sibling::div[1]/text()')
        site_address_text = tree.xpath('(//div[@class="sectionSubTitle"])[2]/following-sibling::div/text()')
        classification_text = tree.xpath('(//div[@class="sectionSubTitle"])[4]/following-sibling::div/text()')
        sales_text = self.xpath(tree, '//table[@class="detailsTable"]/tr[2]/td/text()')
        if len(sales_text) < 2:
            sales_text = [" ", " "]
        return {
            "Owner Name": " ".join(owner_text[:-2]),
            "Owner Address": " ".join(owner_text[-2:]),
            "Site Address": " ".join(site_address_text),
            "Classification": " ".join(classification_text),
            "Sale Date": sales_text[1],
            "Sale Price": sales_text[0],
        }

    def getFolioId(self, folio):
        self.log.info("Searching for %s", folio)
        page = self.requestPost(
            "http://www.leepa.org/Search/PropertySearch.aspx",
            data={
                "__VIEWSTATE": self.viewstate,
                "ctl00$BodyContentPlaceHolder$STRAPTextBox": folio,
                "ctl00$BodyContentPlaceHolder$SubmitPropertySearch": "Search"
            }
        )
        match = re.search(r"FolioID=(\d*)", page.text)
        if match:
            self.log.info("Folio ID found %s -> %s", folio, match.group(1))
            return match.group(1)
        else:
            self.log.warning("No match found for %s", folio)

    def requestPost(self, *args, **kwargs):
        h = kwargs.get("headers", {})
        h.update({'User-Agent': ("User-Agent: Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:57.0) "
                                 "Gecko/20100101 Firefox/57.0"),
                  "X-MicrosoftAjax": "Delta=true",
                  })
        kwargs["headers"] = h
        page = ''
        while page == '':
            try:
                page = requests.post(*args, **kwargs)
            except requests.exceptions.ConnectionError:
                self.log.error("Server refused connection, retrying...")
                sleep(1)
        return page

    def setQueryCategory(self, cat):
        pass
