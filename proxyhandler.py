# -*- coding: utf-8 -*-
import random
import requests
from requests.exceptions import ProxyError, ConnectionError

import logging
logging.basicConfig(level=logging.INFO, format='%(asctime)s %(name)s [%(levelname)s] - %(message)s')
logging.getLogger("requests").setLevel(logging.WARNING)

class ProxyHandler(object):
	"""Proxy Handler module for scrapers"""

	def __init__(self, proxylist=None, requestfilter=None, retrylimit=5, successlimit=2000, loghandler=None):
		super(ProxyHandler, self).__init__()
		self.log = logging.getLogger("ProxyHandler")
		if loghandler:
			self.log.addHandler(loghandler)
		self.isAvailable = True
		self.retry_limit = retrylimit
		self.success_limit = successlimit
		self.request_filter = requestfilter
		self.proxy_dict = {}
		if proxylist:
			self.load_proxy_list(proxylist)

	def load_proxy_list(self, proxylist):
		for proxy in proxylist:
			self.proxy_dict[proxy] = {"failure": 0, "success": 0}

	def request_filter(self, req):
		pass

	def request_get(self, *args, **kwargs):
		proxy_address = self.get_next_proxy()
		print self.proxy_dict[proxy_address]
		if not proxy_address:
			return
		h = kwargs.get("headers", {})
		h.update({'User-Agent': (
					"Mozilla/5.0 (Windows NT 10.0; Win64; x64) "
					"AppleWebKit/537.36 (KHTML, like Gecko) "
					"Chrome/56.0.2924.87 Safari/537.36")})
		kwargs["headers"] = h
		kwargs["proxies"] = {
			'http': 'http://{}'.format(proxy_address),
			'https': 'http://{}'.format(proxy_address),
			}
		kwargs["timeout"] = 10
		try:
			r = requests.get(*args, **kwargs)
		except (ProxyError, ConnectionError) as e:
			self.log.debug(e)
			self.set_failure(proxy_address)
			return self.request_get(*args, **kwargs)
		result = self.request_filter(r)
		if result is "OK":
			self.set_success(proxy_address)
			return r
		elif result is "LIMIT_EXCEEDED":
			self.del_proxy(proxy_address)
		else:
			self.set_failure(proxy_address)
		return self.request_get(*args, **kwargs)

	def set_success(self, proxy):
		success_count = self.proxy_dict[proxy]["success"]
		if success_count > self.success_limit:
			self.del_proxy(proxy)
		else:
			self.proxy_dict[proxy]["success"] = success_count + 1

	def set_failure(self, proxy):
		failure_count = self.proxy_dict[proxy]["failure"]
		if failure_count > self.retry_limit:
			self.del_proxy(proxy)
		else:
			self.proxy_dict[proxy]["failure"] = failure_count + 1

	def get_next_proxy(self):
		next_choice = random.choice(self.proxy_dict.keys())
		return next_choice

	def del_proxy(self, proxy):
		del self.proxy_dict[proxy]
		if not len(self.proxy_dict):
			set.isAvailable = False

	def copy(self):
		ph = ProxyHandler()
		ph.proxy_dict = self.proxy_dict.copy()
		return ph