# -*- coding: utf-8 -*-
from gevent import monkey, pool
monkey.patch_all()

import requests
import re
import openpyxl
from lxml import html
import sys
import time
import os

from basescraper import BaseScraper
from dbr import DBRScraper
from countytaxes import CountyTaxes

import argparse
parser = argparse.ArgumentParser(description="Miamidade Scraper", prog=os.path.basename(sys.argv[0]))
parser.add_argument('--start', action="store", help="Start date of search scope.", metavar="MM/DD/YYYY")
parser.add_argument('--end', action="store", help="End date of search scope.", metavar="MM/DD/YYYY")
parser.add_argument('--output', action="store", help ="Output file name with extension.", metavar="out.xlsx")

import logging
logging.basicConfig(level=logging.INFO, format='%(asctime)s %(name)s [%(levelname)s] - %(message)s')
logging.getLogger("requests").setLevel(logging.WARNING)


def setArguments(*args):
	args = parser.parse_args(*args)
	if len(sys.argv)==1:
		parser.print_help()
	if not args.start:
		args.start = raw_input("Enter start date:")
	if not args.end:
		args.end = raw_input("Enter end date:")
	if not args.output:
		args.output = raw_input("Enter the name for output file:")
	return args

class MiamiDadeScraper(BaseScraper):
	"""docstring for MiamidadeScraper"""
	def __init__(self, loghandler=None, proxyhandler=None):
		super(MiamiDadeScraper, self).__init__(loghandler=loghandler, proxyhandler=proxyhandler)
		self.countytaxes = CountyTaxes("miamidade", loghandler=loghandler, proxyhandler=proxyhandler)
		self.dbrScraper = DBRScraper(pool = self.p, loghandler=loghandler, proxyhandler=proxyhandler)
		self.dbrScraper.setQueryCounty("miamidade")
		# (atex-class:702) atex-paper:MIA unsafe structures
		self.headers = ["Folio", "Owner", "Property Address", "Mailing Address", "Primary Zone", "Primary Land Use",
						"Total Tax", "Deed Date", "Last Date of Sale", "Last Sale Price"]

	def parseFolioNumber(self, entry):
		match = re.search(r"\b\d{2}-?\d{4}-?\d{3}-?\d{4}\b", entry)
		if match:
			return match.group()

	def parseFolioEntry(self, entry):
		return self.parseFolioNumber(entry)

	def requestPropertyInfo(self, folio):
		r = self.requestGet(
				"http://www.miamidade.gov/PApublicServiceProxy/PaServicesProxy.ashx",
				headers = {
					"Referer": "http://www.miamidade.gov/propertysearch/",
					},
				params = {
					"Operation": "GetPropertySearchByFolio",
					"clientAppName": "PropertySearch",
					"folioNumber": folio.replace('-',''),
					},
				)
		return r

	def getPropertyInfo(self, folio):
		self.log.info("Folio No: %s - Getting property info...", folio)
		r = self.requestPropertyInfo(folio)
		filter_result = self.requestFilter(r)
		if filter_result is not "OK":
			raise Exception(filter_result)
		j = r.json()
		data = self.parsePropertyInfo(j)
		return data

	def parsePropertyInfo(self, j):
		jMA = j["MailingAddress"]
		data = {
			"Folio": j["PropertyInfo"]["FolioNumber"],
			"Property Address": j["SiteAddress"][0]["Address"] if j["SiteAddress"] else "",
			"Owner": " | ".join(x["Name"] for x in j["OwnerInfos"]),
			"Mailing Address": "{} / {}, {} {}".format(
					" ".join(jMA["Address%d"%x] for x in range(1,4)).strip(),
					jMA["City"],
					jMA["State"],
					jMA["ZipCode"]),
			"Primary Zone": "{} {}".format(j["PropertyInfo"]["PrimaryZone"], j["PropertyInfo"]["PrimaryZoneDescription"]),
			"Primary Land Use": "{} {}".format(j["PropertyInfo"]["DORCode"], j["PropertyInfo"]["DORDescription"]),
			"Sales": [{"DateOfSale": x["DateOfSale"], "SalePrice": x["SalePrice"]}  for x in j["SalesInfos"]]
			}
		data["Owner"] = data["Owner"].replace("&W","&")
		for x in ["| &", "| &", "| %", "% |"]:
			data["Owner"] = data["Owner"].replace(x,"&")
		if data["Sales"]:
			data["Last Date of Sale"] = "{0:02}/{1:02}/{2}".format(*[int(x) for x in data["Sales"][0]["DateOfSale"].split('/')])
			data["Last Sale Price"] = '${:,.0f}'.format(int(data["Sales"][0]["SalePrice"]))
		return data


	def requestFilter(self, req):
		try:
			j = req.json()
		except:
			return "JSON_PARSE"
		if j["Message"]:
			if "Folio not found" in j["Message"]:
				return "NOT_FOUND"
			elif "Request exceeded" in j["Message"]:
				return "LIMIT_EXCEEDED"
		return "OK"

if __name__ == '__main__':
	args = setArguments()#["--start", "02/26/2017", "--end", "02/26/2017", "--output", "miami.xlsx"])
	mds = MiamiDadeScraper()
	mds.setDateRange(args.start, args.end)
	foliolist = mds.getFolioList()
	try:
		mds.getAllInfo(foliolist)
	except:
		mds.log.exception("Got an error during execution. Saving successfully scraped entries...")
	finally:
		mds.saveDataToFile(args.output, mds.scraped_data)