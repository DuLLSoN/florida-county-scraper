# -*- coding: utf-8 -*-
from gevent import monkey, pool

monkey.patch_all()
# internal libs
import requests
import re
import openpyxl
from lxml import html
import sys
import time
import os

# scraper classes
from dbr import DBRScraper
from countytaxes import CountyTaxes
from basescraper import BaseScraper

import logging

logging.basicConfig(level=logging.INFO, format='%(asctime)s %(name)s [%(levelname)s] - %(message)s')
logging.getLogger("requests").setLevel(logging.WARNING)
log = logging.getLogger("BrowardScraper")

import argparse

parser = argparse.ArgumentParser(description="Broward County Scraper", prog=os.path.basename(sys.argv[0]))
parser.add_argument('--start', action="store", help="Start date of search scope.", metavar="MM/DD/YYYY")
parser.add_argument('--end', action="store", help="End date of search scope.", metavar="MM/DD/YYYY")
parser.add_argument('--output', action="store", help="Output file name with extension.", metavar="out.xlsx")


def setArguments(*args):
	args = parser.parse_args(*args)
	if len(sys.argv) == 1:
		parser.print_help()
	if not args.start:
		args.start = raw_input("Enter start date:")
	if not args.end:
		args.end = raw_input("Enter end date:")
	if not args.output:
		args.output = raw_input("Enter the name for output file:")
	return args


class BrowardScraper(BaseScraper):
	"""docstring for BrowardScraper"""

	def __init__(self, loghandler=None, proxyhandler=None):
		super(BrowardScraper, self).__init__(loghandler=loghandler, proxyhandler=proxyhandler)
		self.countytaxes = CountyTaxes("broward", loghandler=loghandler, proxyhandler=proxyhandler)
		self.dbrScraper = DBRScraper(pool=self.p, loghandler=loghandler, proxyhandler=proxyhandler)
		self.dbrScraper.setQueryCounty("broward")
		self.headers = ["Folio", "Property Address", "Mailing Address", "Owners", "Use Code",
						"Deed Date", "Total Tax", "Last Date of Sale", "Last Sale Amount"]

	def parseFolioNumber(self, entry):
		match = re.search(r"\w{6}-\w{2}-\w{4}", entry)
		if match:
			return self.processFolioNumber(match.group())
		else:
			return [self.processFolioNumber(x) for x in re.findall(r"\b\d{4}-\d{2}-\d{4}\b", entry)]

	def parseFolioEntry(self, entry):
		return self.parseFolioNumber(entry)

	def processFolioNumber(self, folio):
		folio = folio.replace(" ","").replace("-","")
		if len(folio) == 9:
			folio = '0'+folio
		if len(folio) == 10:
			if folio[0] in ['0','1']:
				bf = '5'
			elif folio[0] in ['7','8','9']:
				bf = '4'
			bf = bf+folio[0]
			if folio[1] in ['8','9']:
				bf = bf+'3'
			elif folio[1] in ['0','1','2','3']:
				bf = bf+'4'
			folio = bf + folio[1:]
		if folio[0] in ['1']:
			folio = folio[1:]
			if folio[0] in ['7','8','9']:
				bf = '4'
			elif folio[0] in ['0','1']:
				bf = '5'
			bf = bf+folio[0]
			if folio[2] in ['8','9']:
				bf = bf+'3'
			elif folio[2] in ['0','1','2','3']:
				bf = bf+'4'
			folio = bf+folio[1:]
		if len(folio) == 11:
			folio = folio+'0'
		if len(folio) == 12:
			return folio



	def getPropertyInfo(self, folio):
		if len(folio) < 12:
			folio = self.processFolioNumber(folio)
		self.log.info("Folio No: %s - Getting property info...", folio)
		r = self.requestGet("http://www.bcpa.net/RecInfo.asp?URL_Folio={}".format(folio.replace('-', '')))
		filter_result = self.requestFilter(r)
		if filter_result is not "OK":
			raise Exception(filter_result)
		t = html.fromstring(r.text)
		xp = lambda x: self.xpath(t, x)
		data = {
			"Folio": xp('//td[span[contains(text(),"ID #")]]/following-sibling::td/span/text()')
				.replace(u'\xa0', ''),
			"Property Address": ", ".join(
				xp('//td[span[contains(text(),"Site Address")]]/following-sibling::td/span/a/b/text()')),
			"Mailing Address": xp('//td[span[contains(text(),"Mailing Address")]]/following-sibling::td/span/text()'),
			"Owners": " & ".join(xp('//td[span[contains(text(),"Property Owner")]]/following-sibling::td/span/text()')),
			"Use Code": xp('//td[*/*[contains(text(),"Use")]]/following-sibling::td/span/text()')[0:2],
			"Last Date of Sale": "",
			"Last Sale Amount": xp(
				'//tr[*/*[contains(text(),"Sales History")]]/following-sibling::tr[2]/td[3]/span/text()'),
		}
		prevsaledate = xp('//tr[*/*[contains(text(),"Sales History")]]/following-sibling::tr[2]/td[1]/span/text()')
		if prevsaledate:
			data["Last Date of Sale"] = "{0:02}/{1:02}/{2}".format(*[int(x) for x in prevsaledate.split('/')])
		data["Owners"] = data["Owners"].replace("& %", "&").replace("& &", "&")
		return data

	def requestFilter(self, req):
		if "Broward County Property Appraiser" not in req.text:
			return "BAD_RESPONSE"
		elif "ID #" not in req.text:
			return "NO_DATA"
		return "OK"


p = pool.Pool(6)
if __name__ == '__main__':
	args = setArguments()  # ["--start", "01/01/2017", "--end", "01/25/2017", "--output", "output.xlsx"])
	scraper = BrowardScraper()
	scraper.setDateRange(args.start, args.end)
	foliolist = scraper.getFolioList()
	try:
		scraper.getAllInfo(foliolist)
	except:
		log.exception("Got an error during execution. Saving successfully scraped entries...")
	finally:
		scraper.saveDataToFile(args.output, scraper.scraped_data)
	time.sleep(5)
