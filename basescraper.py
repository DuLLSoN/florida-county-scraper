# -*- coding: utf-8 -*-
from gevent import monkey, pool, sleep

monkey.patch_all()

import logging
import openpyxl
import requests
import os

logging.basicConfig(level=logging.INFO, format='%(asctime)s %(name)s [%(levelname)s] - %(message)s')
logging.getLogger("requests").setLevel(logging.WARNING)


class BaseScraper(object):
	"""docstring for BaseScraper"""

	def __init__(self, loghandler=None, proxyhandler=None):
		super(BaseScraper, self).__init__()
		self.log = logging.getLogger(self.__class__.__name__)
		self.scraped_data = {}
		if loghandler:
			self.log.addHandler(loghandler)
		if proxyhandler:
			self.setProxyHandler(proxyhandler)
			self.p = pool.Pool(30)
		else:
			self.p = pool.Pool(10)

	def setQueryCategory(self, cat):
		self.queryCategory = cat
		self.dbrScraper.setQueryCategory(cat)
		self.dbrScraper.buildQuery()

	def getAllInfo(self, foliolist, action=None):
		self.scraped_data = {}
		try:
			for item in self.p.imap_unordered(self.getPropertyInfoAndTax, foliolist):
				if action:
					action()
				if item:
					f = item["Folio"]
					self.scraped_data[f] = item
		except:
			self.p.kill()

	def getPropertyInfoAndTax(self, folio):
		try:
			data = {}
			data.update(self.getPropertyInfo(folio))
			data.update(self.countytaxes.getPropertyTax(folio))
			return data
		except Exception as e:
			if e.message == "LIMIT_EXCEEDED":
				raise
			elif e.message == "REQUEST_NONE":
				if self.proxyhandler and not self.proxyhandler.isAvailable:
					self.log.error("Proxy list is empty!")
					raise
			elif e.message == "NO_DATA":
				self.log.error("Found no result %s", folio)
				return None
			self.log.exception("Program encountered an error while scraping folio: %s", folio)
			return None

	def requestGet(self, *args, **kwargs):
		h = kwargs.get("headers", {})
		h.update({'User-Agent': (
			"Mozilla/5.0 (Windows NT 10.0; Win64; x64) "
			"AppleWebKit/537.36 (KHTML, like Gecko) "
			"Chrome/56.0.2924.87 Safari/537.36")})
		kwargs["headers"] = h
		page = ''
		while page == '':
			try:
				page = requests.get(*args, **kwargs)
			except requests.exceptions.ConnectionError:
				self.log.error("Server refused connection, retrying...")
				sleep(1)
		return page

	def readFolioListFromExcel(self, file):
		wb = openpyxl.load_workbook(file)
		ws = wb.active
		data = set()
		for row in ws.iter_rows(row_offset=1):
			data.add(row[0].value)
		return data

	def saveDataToFile(self, file, scraped_data):
		if not len(scraped_data):
			return
		if os.path.isfile(file):
			self.log.info("Updating output file: %s", file)
			wb = openpyxl.load_workbook(file)
		else:
			self.log.info("Creating output file: %s", file)
			wb = openpyxl.Workbook()
			wb.active.append(self.headers)
		ws = wb.active
		for k, v in scraped_data.iteritems():
			rowdata = [v.get(x, " ") for x in self.headers]
			ws.append(rowdata)
		self.xlsFixWidth(ws)
		wb.save(file)
		self.log.info("Successfully saved %d entries.", len(scraped_data))

	def xlsFixWidth(self, ws):
		dims = {}
		for row in ws.rows:
			for cell in row:
				if cell.value:
					dims[cell.column] = max((dims.get(cell.column, 0), len(cell.value)))
		for col, value in dims.items():
			if value > 100:
				value = 100
			ws.column_dimensions[col].width = value

	def setProxyHandler(self, ph):
		self.proxyhandler = ph
		self.requestGet = self.proxyhandler.request_get
		self.proxyhandler.request_filter = self.requestFilter

	def setDateRange(self, start_date, end_date):
		self.dbrScraper.setParameterDate(start_date, end_date)
		self.log.info("Search date range: %s - %s", start_date, end_date)

	def getFolioList(self):
		self.dbrScraper.getDBREntryCount()
		entrylist = list(self.dbrScraper.getDBREntryList())
		self.foliolist = set()
		for entry in entrylist:
			result = self.parseFolioEntry(entry)
			if isinstance(result, basestring):
				self.foliolist.add(result)
			elif isinstance(result, list):
				for r in result:
					self.foliolist.add(r)
			else:
				ValueError("Invalid output")
		self.log.info("Filtered %d unique IDs out of %d entries.", len(self.foliolist), len(entrylist))
		return self.foliolist

	def xpath(self, t, xp):
		node = t.xpath(xp)
		if len(node) == 1:
			return node[0].strip()
		elif not node:
			return None
		else:
			return filter(None, [x.strip() for x in node])

	def requestFilter(self, request):
		return "OK"

	def parseName(self, string):
		corp_tokens = [
			"LLC",
			"INC",
			"GROUP",
			"ASSN",
			"LIMITED",
		]
