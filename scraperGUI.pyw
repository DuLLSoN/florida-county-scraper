#!/usr/bin/python
# -*- coding: utf-8 -*-

"""
Scraper GUI 

"""

__version__ = "v1.2.2"

import logging
import os
import sys

import gevent
import pdfquery
from PySide import QtGui, QtCore

from broward import BrowardScraper
from leecounty import LeecountyScraper
from miamidade import MiamiDadeScraper
from palmbeach import PalmbeachScraper
from proxyhandler import ProxyHandler

logging.basicConfig(level=logging.DEBUG, format='%(asctime)s %(name)s [%(levelname)s] - %(message)s')

scrape_modules = {
	"miamidade": {
		"label": "Miami-dade County",
		"class": MiamiDadeScraper,
		"categories": [
			"Tax Deed Applications",
			],
		},
	"broward": {
		"label": "Broward County",
		"class": BrowardScraper,
		"categories": [
			"Tax Deed Applications",
			"Unsafe Structures",
			],
		},
	"palmbeach": {
		"label": "Palm Beach County",
		"class": PalmbeachScraper,
		"categories": [
			"Tax Deed Applications",
			],
		},
    "leecounty": {
        "label": "Lee County",
        "class": LeecountyScraper,
        "categories": [
            "Tax Deed Applications",
        ],
    }
	}

class ScraperGUI(QtGui.QWidget):
	def __init__(self):
		super(ScraperGUI, self).__init__()
		self.initUI()
		self.log = logging.getLogger("UI")
		dummyEmitter = QtCore.QObject()
		self.connect(dummyEmitter, QtCore.SIGNAL("logMsg(QString)"), self.textEdit_log.append)
		self.loghandler = QtCustomLogHandler(dummyEmitter)
		self.loghandler.setLevel(logging.INFO)
		self.loghandler.setFormatter(
			logging.Formatter("%(asctime)s %(name)s [%(levelname)s] - %(message)s", datefmt="%H:%M:%S"))
		self.log.addHandler(self.loghandler)

		def handle_exception(*exc_info):
			self.log.critical("Uncaught exception", exc_info=exc_info)
		sys.excepthook = handle_exception

	def initUI(self):

		formgrid = QtGui.QGridLayout()
		self.setLayout(formgrid)

		self.label_county = QtGui.QLabel("Select County:")
		self.comboBox_scrape_county = QtGui.QComboBox()
		self.comboBox_scrape_county.addItems([v["label"] for k,v in scrape_modules.iteritems()])# ["Miami-Dade", "Broward"])  # , "Palm Beach"])
		self.comboBox_scrape_county.currentIndexChanged.connect(self.comboBoxCountyChanged)
		self.comboBox_scrape_county.setEnabled(True)
		self.label_category = QtGui.QLabel("Select Category:")
		self.comboBox_scrape_category = QtGui.QComboBox()
		self.comboBox_scrape_category.addItems(["Tax Deed Applications"])
		self.comboBox_scrape_category.setEnabled(True)

		self.buttonStart = QtGui.QPushButton("Start Scraping!")
		self.buttonStart.clicked.connect(self.buttonStartClicked)
		self.buttonStart.setSizePolicy(QtGui.QSizePolicy.Expanding, QtGui.QSizePolicy.Expanding)

		self.checkbox_proxylist = QtGui.QCheckBox("Proxy list:")
		self.checkbox_proxylist.stateChanged.connect(self.checkBoxProxyStateChanged)
		self.lineEdit_proxylist = QtGui.QLineEdit()
		self.lineEdit_proxylist.setEnabled(False)
		self.toolButton_proxylist = QtGui.QToolButton()
		self.toolButton_proxylist.setText("...")
		self.toolButton_proxylist.clicked.connect(lambda: self.showFileOpenDialog(
			"Open proxy list",
			"Text files (*.txt)",
			self.lineEdit_proxylist,
		))
		self.toolButton_proxylist.setEnabled(False)

		self.tabFrame = QtGui.QTabWidget()
		self.tabFrame.tabBar().setExpanding(True)
		tab1 = QtGui.QWidget()
		tab1grid = QtGui.QGridLayout()
		tab1.setLayout(tab1grid)
		self.tabFrame.addTab(tab1, "DailyBusinessReview Entry Scraping")
		tab2 = QtGui.QWidget()
		tab2grid = QtGui.QGridLayout()
		tab2.setLayout(tab2grid)
		self.tabFrame.addTab(tab2, "Text file")
		tab3 = QtGui.QWidget()
		tab3grid = QtGui.QGridLayout()
		tab3.setLayout(tab3grid)
		self.tabFrame.addTab(tab3, "PDF file")

		self.dateEdit_start = QtGui.QDateTimeEdit(QtCore.QDate.currentDate(), self)
		self.dateEdit_start.setCalendarPopup(True)
		self.dateEdit_start.setDisplayFormat("MM/dd/yyyy")
		tab1grid.addWidget(QtGui.QLabel("Start Date:"), 0, 0)
		tab1grid.addWidget(self.dateEdit_start, 0, 1)

		tab1grid.setColumnMinimumWidth(2, 50)

		self.dateEdit_end = QtGui.QDateTimeEdit(QtCore.QDate.currentDate(), self)
		self.dateEdit_end.setCalendarPopup(True)
		self.dateEdit_end.setDisplayFormat("MM/dd/yyyy")
		tab1grid.addWidget(QtGui.QLabel("End Date:"), 0, 3)
		tab1grid.addWidget(self.dateEdit_end, 0, 4)

		tab2grid.addWidget(QtGui.QLabel("Folio List:"), 0, 0)
		self.lineEdit_foliolist = QtGui.QLineEdit()
		tab2grid.addWidget(self.lineEdit_foliolist, 0, 1)
		toolButton_foliolist = QtGui.QToolButton()
		toolButton_foliolist.setText("...")
		toolButton_foliolist.clicked.connect(lambda: self.showFileOpenDialog(
			"Open folio list",
			"Text files (*.txt)",
			self.lineEdit_foliolist,
		))
		tab2grid.addWidget(toolButton_foliolist, 0, 2)

		tab3grid.addWidget(QtGui.QLabel("PDF file:"), 0, 0)
		self.lineEdit_pdffile = QtGui.QLineEdit()
		tab3grid.addWidget(self.lineEdit_pdffile, 0, 1)
		toolButton_pdffile = QtGui.QToolButton()
		toolButton_pdffile.setText("...")
		toolButton_pdffile.clicked.connect(lambda: self.showFileOpenDialog(
			"Open PDF file containing folio numbers",
			"PDF files (*.pdf)",
			self.lineEdit_pdffile,
		))
		tab3grid.addWidget(toolButton_pdffile, 0, 2)

		self.label_output = QtGui.QLabel("Output file:")
		self.lineEdit_output = QtGui.QLineEdit()

		toolButton_output = QtGui.QToolButton()
		toolButton_output.setText("...")
		toolButton_output.clicked.connect(lambda: self.showFileSaveDialog(
			"Save output file",
			"Excel files (*.xlsx)",
			self.lineEdit_output,
		))

		self.textEdit_log = QtGui.QTextEdit()
		self.textEdit_log.setReadOnly(True)

		self.progressBar = QtGui.QProgressBar()
		self.progressBar.setFormat(r"%v/%m")

		for i, v in [(0, 0), (1, 1), (2, 1), (3, 1), (4, 1)]:
			formgrid.setColumnStretch(i, v)
		# def addWidget (arg__1, row, column, rowSpan, columnSpan[, alignment=0])
		formgrid.addWidget(self.label_county, 0, 0)
		formgrid.addWidget(self.comboBox_scrape_county, 0, 1, 1, 2)
		formgrid.addWidget(self.label_category, 1, 0)
		formgrid.addWidget(self.comboBox_scrape_category, 1, 1, 1, 2)
		formgrid.addWidget(self.buttonStart, 0, 3, 2, 2)
		formgrid.addWidget(self.checkbox_proxylist, 2, 0)
		formgrid.addWidget(self.lineEdit_proxylist, 2, 1, 1, 3)
		formgrid.addWidget(self.toolButton_proxylist, 2, 4)
		formgrid.addWidget(self.tabFrame, 3, 0, 1, 5)
		formgrid.addWidget(self.label_output, 4, 0)
		formgrid.addWidget(self.lineEdit_output, 4, 1, 1, 3)
		formgrid.addWidget(toolButton_output, 4, 4)
		formgrid.addWidget(self.textEdit_log, 5, 0, 1, 5)
		formgrid.setRowStretch(5, 1)
		formgrid.addWidget(self.progressBar, 6, 0, 1, 5)

		# self.setFixedSize(500, 400)
		self.resize(500, 400)
		self.setWindowTitle('Scraper GUI %s' % __version__)

	def showFileOpenDialog(self, title, filter, lineEdit):
		file, _ = QtGui.QFileDialog.getOpenFileName(self, title, "", filter)
		if file:
			lineEdit.setText(file)

	def showFileSaveDialog(self, title, filter, lineEdit):
		file, _ = QtGui.QFileDialog.getSaveFileName(self, title, "", filter, "", QtGui.QFileDialog.DontConfirmOverwrite)
		if file:
			lineEdit.setText(file)

	def testInputFiles(self):
		mb = QtGui.QMessageBox()
		if not self.lineEdit_output.text():
			mb.setText("Please select an output file.")
			mb.exec_()
			return False
		if self.checkbox_proxylist.isChecked() and not self.lineEdit_proxylist.text():
			mb.setText("Proxylist is selected but empty!")
			mb.exec_()
			return False
		if self.tabFrame.currentIndex() == 1 and not self.lineEdit_foliolist.text():
			mb.setText("Please select a folio list file.")
			mb.exec_()
			return False
		elif self.tabFrame.currentIndex() == 2 and not self.lineEdit_pdffile.text():
			mb.setText("Please select a PDF file.")
			mb.exec_()
			return False
		return True

	def buttonStartClicked(self):
		if not self.testInputFiles():
			return
		self.buttonStart.setEnabled(False)
		self.startScraper(self.comboBox_scrape_county.currentText(), self.comboBox_scrape_category.currentText())
		# if self.comboBox_scrape_county.currentText() == "Miami-Dade":
		# 	self.runMiamiDadeScraper()
		# elif self.comboBox_scrape_county.currentText() == "Broward":
		# 	self.runBrowardScraper()
		self.buttonStart.setEnabled(True)

	def comboBoxCountyChanged(self, state):
		selected = self.comboBox_scrape_county.currentText()
		for k,v in scrape_modules.iteritems():
			if v["label"] == selected:
				key = k
				break
		self.comboBox_scrape_category.clear()
		self.comboBox_scrape_category.addItems(scrape_modules[key]["categories"])
		# if state == 0:
		#    self.comboBox_scrape_category.setEnabled(True)
		# elif state == 1:
		#    self.comboBox_scrape_category.setEnabled(False)

	def checkBoxProxyStateChanged(self, state):
		self.lineEdit_proxylist.setEnabled(state)
		self.toolButton_proxylist.setEnabled(state)

	def readFolioFile(self, filepath):
		data = set()
		with open(filepath, 'r') as file:
			for line in file:
				data.add(line.strip())
		self.log.info("Imported %d folio numbers from text file.", len(data))
		return data

	def readPDFFile(self, filepath, parsefunc):
		self.log.info("Opening pdf file %s", filepath)
		pdf = pdfquery.PDFQuery(filepath)  # , parse_tree_cacher=FileCache("tmp"))
		self.log.info("Parsing pdf file, this may take a minute...")
		gevent.sleep(0.1)
		pdf.load()
		foliolist = set()
		for x in pdf.tree.xpath('//LTTextBoxHorizontal'):
			if x.text:
				match = parsefunc(x.text)
				if match:
					foliolist.add(match)
		return foliolist

	def readProxyList(self, path):
		with open(path, 'r') as f:
			proxy_list = [x.strip() for x in f.readlines()]
		self.log.info("Loaded %d proxies from file.", len(proxy_list))
		return proxy_list

	def startScraper(self, scraper_name, category):
		proxyhandler = None
		if self.checkbox_proxylist.isChecked():
			proxy_list = self.readProxyList(self.lineEdit_proxylist.text())
			proxyhandler = ProxyHandler(proxylist=proxy_list)
		for k,v in scrape_modules.iteritems():
			if v["label"] == scraper_name:
				key = k
				break
		scr = scrape_modules[key]["class"](loghandler=self.loghandler, proxyhandler=proxyhandler)
		scr.setQueryCategory(self.comboBox_scrape_category.currentText())
		self.log = scr.log
		tabindex = self.tabFrame.currentIndex()
		if tabindex == 0:  # DBR scraping
			date_start = self.dateEdit_start.date().toString("MM/dd/yyyy")
			date_end = self.dateEdit_end.date().toString("MM/dd/yyyy")
			scr.setDateRange(date_start, date_end)
			foliolist = scr.getFolioList()
			self.log.info("Scraped %d folio numbers from DailyBusinessReview.", len(foliolist))
		elif tabindex == 1:  # text file parsing
			foliolist = self.readFolioFile(self.lineEdit_foliolist.text())
		elif tabindex == 2:  # pdf parsing
			foliolist = self.readPDFFile(self.lineEdit_pdffile.text(), scr.parseFolioNumber)
		# appending if output file exists
		if os.path.isfile(self.lineEdit_output.text()):
			self.log.info("Output file already exists, filtering already scraped entries...")
			foliolist_old = scr.readFolioListFromExcel(self.lineEdit_output.text())
			foliolist = foliolist - foliolist_old
			self.log.info("Filtered and found %d new entries.", len(foliolist))
		# check if there are folios to be downloaded
		if len(foliolist):
			self.progressBar.setMaximum(len(foliolist))
			self.progressBar.setValue(0)

			def updatePB():
				self.progressBar.setValue(self.progressBar.value() + 1)

			try:
				scr.getAllInfo(foliolist, updatePB)
			except:
				self.log.exception("Got an error during execution. Saving successfully scraped entries...")
			finally:
				scr.saveDataToFile(self.lineEdit_output.text(), scr.scraped_data)
		self.log.info("Scraping completed!")


class QtCustomLogHandler(logging.Handler):
	""" Logging frame to display log details on GUI"""

	def __init__(self, sigEmitter):
		logging.Handler.__init__(self)
		self.sigEmitter = sigEmitter

	def emit(self, record):
		record = self.format(record)
		self.sigEmitter.emit(QtCore.SIGNAL("logMsg(QString)"), record)



class PyQtGreenlet(gevent.Greenlet):
	""" Updating GUI thread while greenlets are working """

	def __init__(self, app):
		gevent.Greenlet.__init__(self)
		self.app = app

	def _run(self):
		while True:
			self.app.processEvents()
			while self.app.hasPendingEvents():
				self.app.processEvents()
				gevent.sleep(0.01)
			gevent.sleep(0.1)


def main():
	app = QtGui.QApplication(sys.argv)
	g = PyQtGreenlet.spawn(app)
	app.setStyle("plastique")
	ex = ScraperGUI()
	ex.show()
	sys.exit(app.exec_())


if __name__ == '__main__':
	main()
