# -*- coding: utf-8 -*-
from gevent import monkey, pool
monkey.patch_all()

import os
import time
import sys
import re
import openpyxl
import pdfquery
from pdfquery.cache import FileCache

import logging
logging.basicConfig(level=logging.INFO, format='%(asctime)s %(name)s [%(levelname)s] - %(message)s')
logging.getLogger("requests").setLevel(logging.WARNING)
log = logging.getLogger("PdfToMiamidadeScraper")

import countytaxes
import miamidade

import argparse
parser = argparse.ArgumentParser(description="PDF Scraper", prog=os.path.basename(sys.argv[0]))
parser.add_argument('--input', action="store", help="Pdf file name with extension containing folio numbers.", metavar="input.pdf")
parser.add_argument('--output', action="store", help ="Output file name with extension.", metavar="output.xlsx")

def setArguments(*args):
	args = parser.parse_args(*args)
	if len(sys.argv)==1:
		parser.print_help()
	if not args.input:
		args.input = raw_input("Input pdf file:")
	if not args.output:
		args.output = raw_input("Output xlsx file:")
	return args

def parsetable(root):
	for x in root.xpath('//LTTextBoxHorizontal'):
		if x.text:
			match = re.search(r"\b(\d{13})\b", x.text)
			if match:
				yield match.group(1)


p = pool.Pool(6)
if __name__ == '__main__':
	args = setArguments()#["--input","Testing.pdf","--output","pdfscrape.xlsx"])
	log.info("Opening pdf file %s", args.input)
	pdf = pdfquery.PDFQuery(args.input)#, parse_tree_cacher=FileCache("tmp"))
	log.info("Parsing pdf file, this may take a minute...")
	pdf.load()
	foliolist = set(parsetable(pdf.tree))
	count = len(foliolist)
	log.info("Filtered %d folio numbers.", count)
	countytaxes.setSection("miamidade")

	scraped_data = {}
	log.info("Scraping Property information.")
	index = 1
	for item in p.imap_unordered(miamidade.getPropertyInfo, list(foliolist)):
		folio_no = item["Folio"]
		log.info("[% 4d/% 4d] Folio No: %s - Getting property info...", index, count, folio_no)
		scraped_data[folio_no] = {}
		scraped_data[folio_no].update(item)
		index += 1
	log.info("Scraping Tax information.")
	index = 1
	for item in p.imap_unordered(countytaxes.getPropertyTax, list(foliolist)):
		folio_no = item["Folio"]
		log.info("[% 4d/% 4d] Folio No: %s - Getting tax info...", index, count, folio_no)
		scraped_data[folio_no].update(item)
		index += 1

	log.info("Download complete. Scraped %d entries.", count)

	log.info("Creating output file: %s", args.output)
	wb = openpyxl.Workbook()
	ws = wb.active
	headers= ["Folio", "Owner", "Property Address", "Mailing Address", "Primary Zone", "Primary Land Use", "Total Tax", "Deed Date", "Previous Sale Date", "Previous Sale Price"]
	ws.append(headers)
	for k, v in scraped_data.iteritems():
		rowdata = [v.get(x," ") for x in headers]
		if v["Sales"]:
			rowdata[8] = "{0:02}/{1:02}/{2}".format(*[int(x) for x in v["Sales"][0]["DateOfSale"].split('/')])
			rowdata[9] = '${:,.0f}'.format(int(v["Sales"][0]["SalePrice"]))
		ws.append(rowdata)
	miamidade.xlsFixWidth(ws)
	wb.save(args.output)
	log.info("Output file created.")
	time.sleep(5)


