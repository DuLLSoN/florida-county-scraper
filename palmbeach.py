# -*- coding: utf-8 -*-
from gevent import monkey, pool

monkey.patch_all()

import re
import openpyxl
from lxml import html
import sys
import time
import os

from basescraper import BaseScraper
from dbr import DBRScraper

import logging

logging.basicConfig(level=logging.INFO, format='%(asctime)s %(name)s [%(levelname)s] - %(message)s')
logging.getLogger("requests").setLevel(logging.WARNING)
log = logging.getLogger("PalmbeachScraper")

import argparse

parser = argparse.ArgumentParser(description="Palmbeach County Scraper", prog=os.path.basename(sys.argv[0]))
parser.add_argument('--start', action="store", help="Start date of search scope.", metavar="MM/DD/YYYY")
parser.add_argument('--end', action="store", help="End date of search scope.", metavar="MM/DD/YYYY")
parser.add_argument('--output', action="store", help="Output file name with extension.", metavar="out.xlsx")


def setArguments(*args):
	args = parser.parse_args(*args)
	if len(sys.argv) == 1:
		parser.print_help()
	if not args.start:
		args.start = raw_input("Enter start date:")
	if not args.end:
		args.end = raw_input("Enter end date:")
	if not args.output:
		args.output = raw_input("Enter the name for output file:")
	return args


def xlsFixWidth(ws):
	dims = {}
	for row in ws.rows:
		for cell in row:
			if cell.value:
				dims[cell.column] = max((dims.get(cell.column, 0), len(cell.value)))
	for col, value in dims.iteritems():
		ws.column_dimensions[col].width = value


class PalmbeachScraper(BaseScraper):
	"""docstring for PalmbeachScraper"""

	def __init__(self, loghandler=None, proxyhandler=None):
		super(PalmbeachScraper, self).__init__(loghandler=loghandler, proxyhandler=proxyhandler)
		self.dbrScraper = DBRScraper(pool=self.p, loghandler=loghandler, proxyhandler=proxyhandler)
		self.dbrScraper.setQueryCounty("palmbeach")
		self.headers = ["Folio", "Property Address", "Mail Address", "Owners", "Use Code", "Zoning",
						"Tax Date", "Tax Amount", "Previous Sale Date", "Previous Sale Amount"]

	def parseFolioNumber(self, entry):
		parcelno = re.search(r"\d{2}-\d{2}-\d{2}-\d{2}-\d{2}-\d{3}-\d{4}", entry)
		return parcelno.group()

	def parseFolioEntry(self, entry):
		parcelno = self.parseFolioNumber(entry)
		date = re.search(r"electronic sale.*?(\d\d.*?\d{4})", entry)
		redemption = re.search(r"REDEMPTION AMOUNT.*?([\d.,]+)", entry)
		data = {
			"Folio": parcelno,
			"Tax Amount": "${}".format(redemption.group(1))
			}
		td = date.group(1)
		for x in ["st", "nd", "rd", "th"]:
			td = td.replace(x, "")
		td = td.replace("<br/>", " ")
		td = time.strptime(td, '%d day of %B, %Y')
		data["Tax Date"] = time.strftime("%m/%d/%Y", td)
		return data

	def getPropertyInfo(self, parcelno):
		self.log.info("Parcel No: %s - Getting property info...", parcelno)
		r = self.requestGet(
			"http://pbcgov.com/papa/Asps/PropertyDetail/PropertyDetail.aspx?parcel=" + parcelno.replace('-', ''))
		t = html.fromstring(r.text)
		xp = lambda x: self.xpath(t, x)
		data = {
			"Folio": parcelno,
			"Property Address": xp('//*[@id="MainContent_lblLocation"]/text()'),
			"Mail Address": "/".join(xp('//span[contains(@id,"MainContent_lblAddrLine")]/text()')),
			"Owners": "".join(xp('//*[@id="MainContent_gvOwners"]//td/text()')),
			"Use Code": xp('//*[@id="MainContent_lblUsecode"]/text()'),
			"Zoning": xp('//*[@id="MainContent_lblZoning"]/text()'),
			"Previous Sale Date": xp('//*[@id="MainContent_gvSalesInfo"]//tr[2]/td[1]/text()'),
			"Previous Sale Amount": xp('//*[@id="MainContent_gvSalesInfo"]//tr[2]/td[2]/text()'),
		}
		return data

	def getAllInfo(self, foliolist, action=None):
		self.scraped_data = {}
		try:
			for item in self.p.imap_unordered(self.getPropertyInfo, foliolist):
				if action:
					action()
				if item:
					f = item["Folio"]
					self.scraped_data[f] = item
					self.scraped_data[f].update(self.dbr_tax_data[f])
		except:
			self.p.kill()

	def getFolioList(self):
		self.dbrScraper.getDBREntryCount()
		entrylist = list(self.dbrScraper.getDBREntryList())
		parsed_entries = map(self.parseFolioEntry, entrylist)
		self.dbr_tax_data = {x["Folio"]: x for x in parsed_entries}
		self.foliolist = set(self.dbr_tax_data.keys())
		self.log.info("Filtered %d unique IDs out of %d entries.", len(self.foliolist), len(parsed_entries))
		return self.foliolist
