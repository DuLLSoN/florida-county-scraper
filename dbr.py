# -*- coding: utf-8 -*-
import requests

import logging
logging.basicConfig(level=logging.INFO, format='%(asctime)s %(name)s [%(levelname)s] - %(message)s')
logging.getLogger("requests").setLevel(logging.WARNING)

queries = {
	"counties": {
		"miamidade": "MIA",
		"broward": "BRO",
		"palmbeach": "PBC",
		},
	"categories": {
		"Tax Deed Applications": "127",
		"Unsafe Structures": "702",
		},
}

class DBRScraper(object):
	"""docstring for DBRScraper"""
	def __init__(self, pool, loghandler=None, proxyhandler=None):
		super(DBRScraper, self).__init__()
		self.log = logging.getLogger("DBRScraper")
		if loghandler:
			self.log.addHandler(loghandler)
		self.pool = pool
		if proxyhandler:
			self.setProxyHandler(proxyhandler)
		self.parameters = {
			"queryString": "(atex-class:127) atex-paper:PBC", #PBC: Palmbeach MIA: Miamidade
			"startYear": "",
			"startMonth": "",
			"startDay": "",
			"stopYear": "",
			"stopMonth": "",
			"stopDay": "",
			"sortField": "publicnotices",
			"sortOrder": "descending",
			"userId": "NYLuser",
			"start": "1",
			"end": "1"}
		self.query_county = "MIA"
		self.query_category = "127"

	def setProxyHandler(self, ph):
		self.proxyhandler = ph.copy()
		self.requestGet = self.proxyhandler.request_get
		self.proxyhandler.request_filter = self.requestFilter

	def setQuery(self, query):
		self.parameters["queryString"] = query

	def setQueryCounty(self, county):
		self.query_county = queries["counties"][county]

	def setQueryCategory(self, category):
		self.query_category = queries["categories"][category]

	def buildQuery(self):
		self.setQuery("(atex-class:{}) atex-paper:{}".format(self.query_category, self.query_county))


	def setParameterDate(self, start_date, end_date):
			date_start = start_date.split('/')
			date_end  = end_date.split('/')
			self.parameters["startYear"] = date_start[2]
			self.parameters["startMonth"] = date_start[0]
			self.parameters["startDay"] = date_start[1]
			self.parameters["stopYear"] = date_end[2]
			self.parameters["stopMonth"] = date_end[0]
			self.parameters["stopDay"] = date_end[1]

	def getDBREntryCount(self):
		self.parameters["start"] = 1
		self.parameters["end"] = 1
		r = self.requestGet("http://data.smartlitigator.com/DBRDataServer/searchMLPNNew.htm", params=self.parameters)
		j = r.json()
		self.entry_count = int(j["searchresults"]["estimate"])
		self.log.info("Found %d entries.", self.entry_count)
		return int(j["searchresults"]["estimate"])
	
	def getDBREntry(self, parameters):
		r = self.requestGet("http://data.smartlitigator.com/DBRDataServer/searchMLPNNew.htm", params=parameters)
		if not r:
			raise Exception("REQUEST_NONE")
		j = r.json()
		return [item["ADBODY"][0] for item in j["searchresults"]["result"]]
	
	def getDBREntryList(self):
		if not self.entry_count:
			self.getDBREntryCount()
		entrylength = 0
		for item in self.pool.imap(self.getDBREntry, self.iterParameterRange(self.entry_count)):
			entrylength += len(item)
			self.log.info("Scraped %d entries from DailyBusinessReview...", entrylength)
			for entry in item:
				yield entry

	def iterParameterRange(self, count):
		range = min(25, count) or 1
		for x in xrange(0, count, range):
			param = self.parameters.copy()
			param["start"] = x + 1
			param["end"] = x + range
			yield param

	def requestGet(self, *args, **kwargs):
		h = kwargs.get("headers",{})
		h.update({'User-Agent': (
					"Mozilla/5.0 (Windows NT 10.0; Win64; x64) "
					"AppleWebKit/537.36 (KHTML, like Gecko) "
					"Chrome/56.0.2924.87 Safari/537.36")})
		kwargs["headers"] = h
		return requests.get(*args,**kwargs)

	def requestFilter(self, req):
		if "searchresults" in req.text:
			return "OK"